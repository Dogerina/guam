package isis.dankerina.guam.deobfuscator.asm;

import isis.dankerina.guam.deobfuscator.util.DirectedGraph;
import org.objectweb.asm.tree.*;

import java.util.*;

public class BranchedClassNode extends ClassNode {

    public final List<BranchedClassNode> supers = new ArrayList<>();
    public final List<BranchedClassNode> delegates = new ArrayList<>();
    public final DirectedGraph<MethodNode, MethodNode> callGraph = new DirectedGraph<>();

    public MethodNode getMethodFromSuper(final String name, final String desc) {
        for (final BranchedClassNode super_ : supers) {
            for (final MethodNode mn : super_.methods) {
                if (mn.name.equals(name) && mn.desc.equals(desc)) {
                    return mn;
                }
            }
        }
        return null;
    }

    public boolean isInherited(final String name, final String desc) {
        return getMethodFromSuper(name, desc) != null;
    }

    public boolean isInherited(final MethodNode mn) {
        return mn.owner.equals(name) && isInherited(mn.name, mn.desc);
    }

    public MethodNode getMethod(final String name, final String desc) {
        for (final MethodNode mn : methods) {
            if (mn.getKey().equals(this.name + '.' + name + desc)) {
                return mn;
            }
        }
        return null;
    }

    public int getLevel() {
        return supers.size();
    }

    public BranchedClassNode getSuperType() {
        return supers.size() > 0 ? supers.get(0) : null; //direct superclass is always added first
    }
}
