package isis.dankerina.guam.deobfuscator.asm;

import java.io.IOException;

import org.objectweb.asm.ClassReader;
import org.topdank.byteengineer.commons.asm.ASMFactory;

public class BranchedClassNodeFactory implements ASMFactory<BranchedClassNode>{

	@Override
	public BranchedClassNode create(byte[] bytes, String name) throws IOException {
		ClassReader cr = new ClassReader(bytes);
		BranchedClassNode cn = new BranchedClassNode();
		cr.accept(cn, ClassReader.SKIP_FRAMES | ClassReader.SKIP_DEBUG);
		return cn;
	}
}