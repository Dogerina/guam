package isis.dankerina.guam.deobfuscator.visitor;

import isis.dankerina.guam.deobfuscator.asm.BranchedClassNode;
import isis.dankerina.guam.deobfuscator.util.TimedTask;

import java.util.*;

public abstract class Visitor extends TimedTask {

    protected final Map<String, BranchedClassNode> classes;
    protected final List<String> results;

    public Visitor(final TimedTask delegate, final Map<String, BranchedClassNode> classes) {
        super(delegate);
        this.results = new ArrayList<>();
        this.classes = classes;
    }

    public Visitor(final Map<String, BranchedClassNode> classes) {
        this(null, classes);
    }

    public final void onFinish() {
        final StringBuilder sb = new StringBuilder();
        sb.append("-> Ran ").append(getClass().getSimpleName()).append(" in ").append(super.toString()).append("\n");
        for (final String result : results)
            sb.append("^-> ").append(result).append("\n");
        System.out.println(sb.toString());
    }
}
