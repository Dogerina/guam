package isis.dankerina.guam.deobfuscator.visitor.impl;

import isis.dankerina.guam.deobfuscator.asm.BranchedClassNode;
import isis.dankerina.guam.deobfuscator.visitor.Visitor;

import java.util.*;

public class HierarchyVisitor extends Visitor {

    public HierarchyVisitor(final Map<String, BranchedClassNode> classes) {
        super(classes);
    }

    @Override
    protected void execute() {
        for (final BranchedClassNode node : classes.values()) {
            for (final String iface : node.interfaces) {
                final BranchedClassNode ifacecs = classes.get(iface);
                if (ifacecs != null) {
                    ifacecs.delegates.add(node);
                    final Collection<BranchedClassNode> superinterfaces = new ArrayList<>();
                    visitImpl(classes, superinterfaces, ifacecs);
                    node.supers.addAll(superinterfaces);
                }
            }
            BranchedClassNode currentSuper = classes.get(node.superName);
            while (currentSuper != null) {
                currentSuper.delegates.add(node);
                node.supers.add(currentSuper);
                for (final String iface : currentSuper.interfaces) {
                    final BranchedClassNode ifacecs = classes.get(iface);
                    if (ifacecs != null) {
                        ifacecs.delegates.add(currentSuper);
                        final Collection<BranchedClassNode> superinterfaces = new ArrayList<>();
                        visitImpl(classes, superinterfaces, ifacecs);
                        currentSuper.supers.addAll(superinterfaces);
                        node.supers.addAll(superinterfaces);
                    }
                }
                currentSuper = classes.get(currentSuper.superName);
            }
        }
        results.add("Graph built for " + classes.size() + " classes!");
    }

    private void visitImpl(final Map<String, BranchedClassNode> classes, final Collection<BranchedClassNode> superinterfaces,
                           final BranchedClassNode current) {
        superinterfaces.add(current);
        for (final String iface : current.interfaces) {
            final BranchedClassNode cs = classes.get(iface);
            if (cs != null) {
                cs.delegates.add(current);
                visitImpl(classes, superinterfaces, cs);
            }
        }
    }
}
