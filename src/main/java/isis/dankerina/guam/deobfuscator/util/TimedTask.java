package isis.dankerina.guam.deobfuscator.util;

import java.util.concurrent.atomic.AtomicLong;

public abstract class TimedTask implements Runnable {

    private final AtomicLong duration;
    private final TimedTask delegate;

    public TimedTask(final TimedTask delegate) {
        this.duration = new AtomicLong(0);
        this.delegate = delegate;
    }

    public TimedTask() {
        this(null);
    }

    public long getDuration() {
        return duration.get();
    }

    protected abstract void execute();

    @Override
    public final void run() {
        duration.set(System.nanoTime());
        execute();
        if (delegate != null)
            delegate.execute();
        duration.set(System.nanoTime() - duration.get());
    }

    @Override
    public String toString() {
        return String.format("%.2f seconds", getDuration() / 1e9);
    }
}
