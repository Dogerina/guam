package isis.dankerina.guam.deobfuscator;

import isis.dankerina.guam.deobfuscator.asm.BranchedClassNode;
import isis.dankerina.guam.deobfuscator.asm.BranchedClassNodeFactory;
import isis.dankerina.guam.deobfuscator.visitor.Visitor;
import isis.dankerina.guam.deobfuscator.visitor.impl.HierarchyVisitor;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.asm.tree.ClassNode;
import org.topdank.byteengineer.commons.data.JarContents;
import org.topdank.byteengineer.commons.data.JarContents.ClassNodeContainer;
import org.topdank.byteengineer.commons.data.JarContents.ResourceContainer;
import org.topdank.byteengineer.commons.data.JarInfo;
import org.topdank.byteio.in.SingleJarDownloader;
import org.topdank.byteio.out.CompleteJarDumper;

public class Application {

	public static Switches SWITCHES = new Switches();

	private static final String DEFAULT_INPUT_NAME = "76.jar";

	public static void main(String[] args) throws IOException {
		SWITCHES.parse(args);

		String inputName = SWITCHES.get("-input", "/" + DEFAULT_INPUT_NAME);
		URL input = Application.class.getResource(inputName);
		File output = new File(SWITCHES.get("-output", "out/dump" + DEFAULT_INPUT_NAME));

		SingleJarDownloader<BranchedClassNode> downloader = new SingleJarDownloader<BranchedClassNode>(new BranchedClassNodeFactory(), new JarInfo(input));
		downloader.download();

		Map<String, BranchedClassNode> classes = downloader.getJarContents().getClassContents().namedMap();
		Visitor[] visitors = { new HierarchyVisitor(classes) };
		for (Visitor visitor : visitors) {
			visitor.run();
			visitor.onFinish();
		}

		ClassNodeContainer<ClassNode> container = new ClassNodeContainer<ClassNode>();
		container.addAll(classes.values());
		new CompleteJarDumper(new JarContents<ClassNode>(container, new ResourceContainer(downloader.getJarContents().getResourceContents()))).dump(output);
	}

	public static class Switches {

		private static final List<String> BOOLEAN_FLAGS = new ArrayList<String>();
		private static final List<String> INT_FLAGS = new ArrayList<String>();
		private static final List<String> FLOAT_FLAGS = new ArrayList<String>();

		private final Map<String, Object> map;

		public Switches() {
			map = new HashMap<String, Object>();
		}

		void parse(String[] args) {
			for (String arg : args) {
				if(!arg.startsWith("-"))
					continue;
				if (!arg.contains("="))// ignore
					continue;
				String[] parts = arg.split("=");
				String key = parts[0];
				Object val = parts[1];
				try {
					if (BOOLEAN_FLAGS.contains(key)) {
						val = Boolean.parseBoolean((String) val);
					} else if (INT_FLAGS.contains(key)) {
						val = Integer.parseInt((String) val);
					} else if (FLOAT_FLAGS.contains(key)) {
						val = Float.parseFloat((String) val);
					}
					map.put(key, val);
				} catch (Exception e) {
					System.err.println(String.format("Illegal flag (key=%s, val=%s)", key, val));
				}
			}
		}

		@SuppressWarnings("unchecked")
		public <T> T get(String key, T defaultVal) {
			Object o = map.get(key);
			if (o == null)
				return defaultVal;
			try {
				return (T) o;
			} catch (ClassCastException e) {
				e.printStackTrace();
				return defaultVal;
			}
		}

		public void substitute(String key, Object o) {
			map.put(key, o);
		}
	}
}