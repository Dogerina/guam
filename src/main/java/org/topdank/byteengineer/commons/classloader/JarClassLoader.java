package org.topdank.byteengineer.commons.classloader;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;
import org.topdank.byteengineer.commons.asm.ASMFactory;
import org.topdank.byteengineer.commons.asm.DefaultASMFactory;
import org.topdank.byteengineer.commons.data.LocateableJarContents;

import sun.misc.URLClassPath;

/**
 * Specific {@link ClassLoader} for loading things from external JarFiles, caching loaded classes as
 * it goes along. <br>
 *
 * @author Bibl
 */
public class JarClassLoader extends ClassLoader {

	/** Associated path for classes **/
	protected LocateableJarContents<?> contents;
	protected ASMFactory<? extends ClassNode> factory;
	protected Map<String, ClassNode> fastNodeCache;
	/** Previously loaded classes **/
	protected HashMap<String, Class<?>> cache;
	protected URLClassPath ucp;

	/**
	 * @param contents Reference to the JarContents object.
	 */
	public JarClassLoader(LocateableJarContents<?> contents) {
		this(contents, new DefaultASMFactory());
	}

	/**
	 * @param contents Reference to the JarContents object.
	 * @param factory ASMFactory.
	 */
	public JarClassLoader(LocateableJarContents<?> contents, ASMFactory<? extends ClassNode> factory) {
		this.contents = contents;
		this.factory = factory;
		cache = new HashMap<String, Class<?>>();
		fastNodeCache = new HashMap<String, ClassNode>(contents.getClassContents().size());
		ucp = new URLClassPath(contents.getJarUrls());
		reload();
	}

	@Override
	public URL findResource(String name) {
		return ucp.findResource(name, true);
	}

	public void reload() {
		for (ClassNode cn : contents.getClassContents()) {
			fastNodeCache.put(cn.name, cn);
		}
	}

	/**
	 * @param cn ClassNode to define
	 * @return Defined Class.
	 */
	public Class<?> defineNode(ClassNode cn) {
		ClassWriter cw = new ClassWriter(0);
		return defineNode(cn, cw);
	}

	/**
	 * Defines a ClassNode with a custom {@link ClassWriter}
	 *
	 * @param cn ClassNode to define
	 * @param cw ClassWriter to define with
	 * @return Defined Class.
	 */
	public Class<?> defineNode(ClassNode cn, ClassWriter cw) {
		cn.accept(cw);
		byte[] bytes = cw.toByteArray();
		Class<?> c = defineClass(null, bytes, 0, bytes.length);
		if (c != null)
			cache.put(cn.name, c);
		fastNodeCache.put(cn.name, cn);
		return c;
	}

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		name = name.replace(".", "/");
		if (cache.containsKey(name))
			return cache.get(name);
		ClassNode node = fastNodeCache.get(name);
		if (node != null)
			return defineNode(node);

		Class<?> c = getSystemClassLoader().loadClass(name);
		if (c != null)
			cacheClass(name, c);
		return c;
	}

	/**
	 * Forces the class to be put into the cache overwriting the previously loaded one.
	 *
	 * @param name Fully qualified name of the class.
	 * @param c Class to add.
	 */
	public void cacheClass(String name, Class<?> c) {
		if ((c != null) && !c.getName().startsWith("java")) {
			cache.put(name, c);
		}
	}

	public void clearLoadedClassCache() {
		cache.clear();
	}
}